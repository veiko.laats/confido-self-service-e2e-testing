FROM cypress/browsers:latest AS runner

RUN apt-get update && apt-get -y install procps openssl git

RUN mkdir e2e
WORKDIR /e2e

COPY . .

# avoid too many progress messages
# https://github.com/cypress-io/cypress/issues/1243
ENV CI=1 \
    # disable shared memory X11 affecting Cypress v4 and Chrome
    # https://github.com/cypress-io/cypress-docker-images/issues/270
    QT_X11_NO_MITSHM=1 \
    _X11_NO_MITSHM=1 \
    _MITSHM=0 \
    # point Cypress at the /root/cache no matter what user account is used
    # see https://on.cypress.io/caching
    CYPRESS_CACHE_FOLDER=/root/.cache/Cypress \
    CYPRESS_CRASH_REPORTS=0

RUN pwd && ls -la && node -v && cp example.env .env && yarn

RUN yarn cy:reinstall-binaries

CMD ["yarn", "cy:cloud-run"]
