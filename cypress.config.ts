import { defineConfig } from "cypress";
import { cloudPlugin } from "cypress-cloud/plugin";
import * as dotenv from "dotenv";

dotenv.config();

export default defineConfig({
  env: {
    baseUrl: process.env.CYPRESS_BASE_URL,
  },
  video: false,
  reporter: "mochawesome",
  reporterOptions: {
    reportDir: "cypress/reports/json",
    overwrite: false,
    html: false,
    json: true,
  },
  retries: {
    runMode: 2,
    openMode: 0,
  },
  chromeWebSecurity: false,
  scrollBehavior: "center",
  e2e: {
    baseUrl: process.env.CYPRESS_BASE_URL,
    setupNodeEvents(on, config) {
      // https://github.com/bahmutov/cypress-failed-log
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      require("cypress-failed-log/on")(on);
      // https://docs.sorry-cypress.dev/guide/get-started
      return cloudPlugin(on, config);
    },
    specPattern: "cypress/e2e/**/*.cy.ts",
  },
});
