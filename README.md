# Confido Self-Service E2E tests

## 1. Requirements

- Node.js >= 18.16 ([nvm](https://github.com/nvm-sh/nvm) or <https://nodejs.org/en/download/>)
- Git >= v2.23.0 (<https://git-scm.com/>)

## 2. Setup runtime environment variables

Copy `example.env` in the project root to `.env` and edit your preferences.

## 3. Cypress

Running tests

- `yarn cy:run` to run tests as headless.
- `yarn cy:open` to run tests as headed (with visible browser).
- `yarn cy:cloud-run:local` to run tests on local cloud cypress with Sorry Cypress.
- `yarn cy:cloud-run` to run tests on cloud cypress with Sorry Cypress.

### 3.1. Cloud cypress with Sorry Cypress

- Required env variables
  - SORRY_CYPRESS_PROJECT_ID
  - SORRY_CYPRESS_RECORD_KEY
  - SORRY_CYPRESS_CLOUD_SERVICE_URL

Run Sorry Cypress locally with tests:

Create new project called `confido-self-service` at [http://localhost:8081/projects](http://localhost:8081/projects) before running tests.

1. Start sorry-cypress `docker compose -f ./docker-compose.sorry-cypress.yml up`
2. Run tests `yarn cy:cloud-run:local` and view results [http://localhost:8081/projects](http://localhost:8081/projects)