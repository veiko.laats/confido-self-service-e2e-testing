declare namespace Cypress {
  interface Chainable {
    getByDataTestId(selector: string): Cypress.Chainable<JQuery<HTMLElement>>;
    authWithSmartId(idCode: string, timeout?: number): void;
    visitSelfServiceAndAcceptCookie(): void;
    checkHasAuthUserInfo(username: string): void;
    logoutAuthUser(): void;
  }
}
