enum Timeout {
  MEDIUM = 10000,
  LONG = 20000,
}

export default Timeout;
