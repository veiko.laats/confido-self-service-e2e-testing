/// <reference types="cypress" />

import authWithSmartIdCommand from "./command-handlers/authWithSmartIdCommand";
import checkHasAuthUserInfoCommand from "./command-handlers/checkHasAuthUserInfoCommand";
import getByDataTestIdCommand from "./command-handlers/getByDataTestIdCommand";
import logoutAuthUserCommand from "./command-handlers/logoutAuthUserCommand";
import visitSelfServiceAndAcceptCookieCommand from "./command-handlers/visitSelfServiceCommand";

Cypress.Commands.add("getByDataTestId", getByDataTestIdCommand);
Cypress.Commands.add(
  "visitSelfServiceAndAcceptCookie",
  visitSelfServiceAndAcceptCookieCommand,
);
Cypress.Commands.add("authWithSmartId", authWithSmartIdCommand);
Cypress.Commands.add("checkHasAuthUserInfo", checkHasAuthUserInfoCommand);
Cypress.Commands.add("logoutAuthUser", logoutAuthUserCommand);
