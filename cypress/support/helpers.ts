const isVisible = (fieldName: string) =>
  cy.getByDataTestId(fieldName).should("be.visible");

const hasFieldWithValue = (fieldName: string, value: unknown) =>
  isVisible(fieldName).should("have.value", value);

export { isVisible, hasFieldWithValue };
