import Timeout from "../constants";
import { isVisible } from "../helpers";

const authWithSmartIdCommand = (idCode: string, timeout?: Timeout) => {
  isVisible("login").click();

  isVisible("smart-id-tab").click();
  cy.get("#sid-personal-code").type(idCode);
  isVisible("smart-id-login-button").click();
  if (timeout) {
    cy.wait(timeout);
  }
};

export default authWithSmartIdCommand;
