const getByDataTestIdCommand = (
  selector: string,
  options?: { timeout?: number },
) => cy.get(`[data-testid="${selector}"]`, options);

export default getByDataTestIdCommand;
