const visitSelfServiceAndAcceptCookieCommand = () => {
  cy.visit(Cypress.env("baseUrl"));
  cy.get("#cookiescript_accept").click();
};

export default visitSelfServiceAndAcceptCookieCommand;
