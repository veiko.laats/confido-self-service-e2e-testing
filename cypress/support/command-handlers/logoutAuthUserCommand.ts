import { isVisible } from "../helpers";

const logoutCommand = () => {
  isVisible("navbar-menu-toggle").click();
  isVisible("logout-button").click();
};

export default logoutCommand;
