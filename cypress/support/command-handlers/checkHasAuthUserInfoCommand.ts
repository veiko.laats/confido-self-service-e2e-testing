import { isVisible } from "../helpers";

const checkHasAuthUserInfoCommand = (username: string) => {
  isVisible("navbar-menu-toggle");
  isVisible("username").should("have.text", username);
};

export default checkHasAuthUserInfoCommand;
