import Timeout from "cypress/support/constants";
import { hasFieldWithValue, isVisible } from "cypress/support/helpers";

import * as authData from "../../fixtures/auth.json";

describe("Auth: Smart-ID", () => {
  beforeEach(() => {
    cy.visitSelfServiceAndAcceptCookie();
  });

  it("should navigate to SSO and show error notice when customer does not have Smart-ID contract", () => {
    const { idCodeWithNoContract, noContractErrorNotice } =
      authData.login.smartId;

    cy.authWithSmartId(idCodeWithNoContract);
    isVisible("smart-id-error-notice").should(
      "have.text",
      noContractErrorNotice,
    );
  });

  it("should navigate to SSO and login with existing Smart-ID user", () => {
    const { idCode, username } = authData.login.smartId;

    cy.authWithSmartId(idCode, Timeout.MEDIUM);
    cy.checkHasAuthUserInfo(username);
    cy.logoutAuthUser();
  });

  // TODO: needs backend logic to delete existing user.
  it.skip("should navigate to SSO and register new user with Smart-ID", () => {
    const { idCode, fistName, lastName, email, phone } =
      authData.register.smartId;

    cy.authWithSmartId(idCode, Timeout.MEDIUM);

    isVisible("registration-form-title");
    hasFieldWithValue("registration-form-firstname", fistName);
    hasFieldWithValue("registration-form-lastname", lastName);
    hasFieldWithValue("registration-form-personalId", idCode);

    isVisible("registration-form-email").type(email);
    isVisible("registration-form-phone").type(phone);
    isVisible("registration-form-language-code").click();
    isVisible("registration-form-language-code-et").click();

    const submitButton = isVisible("registration-form-submit-button").as(
      "submit-button",
    );
    submitButton.should("have.attr", "disabled");
    isVisible("registration-form-agree-terms").check();
    submitButton.should("not.have.attr", "disabled");
    cy.get("@submit-button").click();

    cy.checkHasAuthUserInfo(fistName);
    cy.logoutAuthUser();
  });
});
