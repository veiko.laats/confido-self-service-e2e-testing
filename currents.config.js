const projectId = process.env.SORRY_CYPRESS_PROJECT_ID; // the projectId, can be any values for sorry-cypress users
const recordKey = process.env.SORRY_CYPRESS_RECORD_KEY; // the record key, can be any value for sorry-cypress users
const cloudServiceUrl = process.env.SORRY_CYPRESS_CLOUD_SERVICE_URL; // Sorry Cypress users - set the director service URL

const config = {
  projectId,
  recordKey,
  cloudServiceUrl,
};

console.info(
  `Cypress-cloud configuration = ${JSON.stringify(config, undefined, 2)}`,
);

module.exports = config;
